#! /bin/bash

# dnf5 enables progress bars as it detects an interactive terminal. This
# sometimes causes dnf to SIGABRT. Redirect output to fix this.
exec &> >(tee /tmp/get-docker.log || true)

# Try to install and start node-exporter, but continue if it fails
sudo dnf install -y node-exporter
sudo systemctl enable --now prometheus-node-exporter

set -euo pipefail
sudo dnf install -y moby-engine
